import scrapy
from scrapy import Item,Field
from .. import db
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst
import pandas as pd

class AItem(Item):

    url = Field()
    author_source=Field(output_processor=TakeFirst())
    name=Field()
    domain=Field()
    links=Field()
class JerrySpider(scrapy.Spider):
    name = 'jerry'
    custom_settings = {
        "COOKIES_ENABLED": 0,
        "DOWNLOAD_DELAY": 20,
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    }
    def start_requests(self):
        df=pd.read_csv('https://s3-ap-northeast-1.amazonaws.com/scrapinghub-public-files/jerry.csv')
        for i in range(len(df)):
            item=AItem()
            item['name']=df['author_source'].iloc[i]
            item['url']=df['url'].iloc[i]
            r=scrapy.Request(url=item['url'],callback=self.article_parse)
            r.meta['item']=item
            yield r
    def article_parse(self,response):

        item=response.meta['item']
        il = ItemLoader(item=item, response=response)


        RULES = ['a.author', 'a[rel=author]', 'a[href*=author]',
                 '.post-author .fn', '.byline a', '.author-name',
                 '.c-byline__item a', 'span.author', '.post-author > .fn',
                 'a[itemprop=author]', '.author a']
        # author_schema = './/*[contains(@itemtype, "Person")]/node()'
        author_schema = './/*[contains(@itemprop, "author")]/node()'
        author_meta = '//meta[contains(@property, "aritcle:publisher")]/@content'

        #====================找下一頁 url====================================
        author_class = '//*[contains(@class, "author")]/node()'
        if response.xpath(author_schema):
            il.add_xpath('author_source', '%s/@href'% author_schema)
        elif response.xpath(author_meta):
            il.add_xpath('author_source', '%s/@href'% author_meta)
        else:
            for name in RULES:

                if response.css('%s::text' %name):

                    il.add_css('author_source','%s::attr(href)' %name)
                    break

                elif name=='.author':
                     if aresponse.xpath(author_class):
                        il.add_xpath('author_source', '%s/@href' % author_class)
                        break


        domain=il.load_item()['url'].split('/')[2]
        item['domain']=domain
        link=il.load_item()['author_source']
        link = link if link.startswith('http') else 'http://' + domain + link

        r=scrapy.Request(url=link,callback=self.parse_author_profile)
        r.meta['item']=il.load_item()
        yield r

    def parse_author_profile(self, response):

        def is_social_link(target):
            SOCIAL_DOMAINS = ['facebook.com', 'twitter.com', 'linkedin.com']
            if not target:
                return False
            return any([target.find(sdomain) != -1 for sdomain in SOCIAL_DOMAINS])

        item = response.meta['item']
        #===================比對是否為domain   social media
        def is_company(link,domain):
            if link.find(domain.split('.')[1])!=-1 or not link:
                return False
            return True


        links = set([target for target in response.css('a::attr(href)').extract() if is_social_link(target) and is_company(target,item['domain']) ])
        links_str = ':::'.join(links)
        item['links']=links_str
        yield item
