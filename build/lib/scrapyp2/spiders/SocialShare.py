import scrapy
import json
from .. import db

from scrapy import Item,Field

class SShareItem(Item):

    url = Field()
    fb = Field()
    google = Field()
    twitter = Field()
    linkedin=Field()
    url_id=Field()


class SocialShareSpider(scrapy.Spider):
    name = 'socialshare'
    custom_settings = {
        "COOKIES_ENABLED": 0,
        #It's second delay
        "DOWNLOAD_DELAY": 0.75,
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    }
    def start_requests(self):
        rows=db.query("select url,id from amplify.articles where twitter is null limit 5000")
        for r in rows:
            api_url='http://free.donreach.com/shares?providers=facebook,google,twitter,linkedin&url='
            headers={
            'Authorization':'26062b538e93459e73b39702128b27c7'

            }
            item=SShareItem()
            item['url_id']=r[1]
            r=scrapy.Request(url=api_url+r[0],headers=headers,callback=self.parse)
            r.meta['item']=item
            yield r

    def parse(self,response):

        item=response.meta['item']
        print('============reult====================')
        print(response.body)
        sshare=json.loads(response.body)


        fb=sshare['shares']['facebook']
        google=sshare['shares']['google']
        twitter=sshare['shares']['twitter']
        linkedin=sshare['shares']['linkedin']



        item['fb']=fb
        item['twitter']=twitter
        item['google']=google
        item['linkedin']=linkedin
        db.query("""update amplify.articles
                    set fb=%s,twitter=%s,google=%s,linkedin=%s where id=%s"""
                    %(fb,twitter,google,linkedin,item['url_id']))
        yield item
