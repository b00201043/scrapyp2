#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    //  CHi 20171014 ---
    article rule
        a) title
            1) meta > property > og:title
            2) title | h1
        b) content
            1) @class | @id == contnet
        c) image
            meta > property > og:image
        d) publish date
            meta > property > article:published_time


    -- UnicodeEncodeError - \xa043
        using w3lib
        re.sub(r'[^\x00-\x7F]+',' ', text)
    -- regex_sentence
        ([A-Z][^\.!?]*[\.!?])
"""
import re
import scrapy
from scrapy.spiders import Spider
from scrapy.http import Request
from scrapy.item import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from w3lib.html import remove_tags, replace_entities, replace_escape_chars

import pymysql


def query(sql, params=()):
    # decode oarams first
    db = pymysql.connect(host='spark.cippsw3zz0rz.ap-northeast-1.rds.amazonaws.com', port=3306, user='duke',  passwd='spark2266', charset='utf8')
    cursor = db.cursor()
    cursor.execute(sql, params)
    db.commit()
    row = cursor.fetchone()
    while row is not None:
        yield row
        row = cursor.fetchone()
    cursor.close()
    db.close()
    yield None

def clean_ascii(value):
    clean_ascii = r'[^\x00-\x7f]+'
    rtn_string = re.sub(clean_ascii, r'', value)
    return rtn_string


def normal_content(value):
    regex_sentence = r'([A-Z][^\.!?]*[\.!?])'
    rtn_string = re.findall(regex_sentence, value)
    return rtn_string


class EasyArticleItem(Item):
    image_url = Field(output_processor=TakeFirst())
    url=Field()
    title = Field(
        input_processor=MapCompose(remove_tags, replace_entities, replace_escape_chars,
                                   clean_ascii),
        output_processor=Join()
    )
    publish_date = Field()
    content = Field(
        input_processor=MapCompose(remove_tags, replace_entities,
                                   replace_escape_chars,
                                   clean_ascii, normal_content
                                   ),
        output_processor=Join()
    )


class EasyArticleSpider(Spider):
    name = 'EZArticle'



    # allowed_domains = ['']

    def start_requests(self):
        print('start requests')

        rows=query('select url from amplify.articles limit 10000,2000;')
        print(rows)
        for url in rows:
            item = EasyArticleItem()
            r=scrapy.Request(url=url[0],callback=self.parse)
            item['url']=url[0]
            r.meta['item'] = item
            yield r

    def parse(self, response):
        item = response.meta['item']

        il = ItemLoader(item=item, response=response)

        # image url
        il.add_xpath('image_url', '//meta[contains(@property, "og:image")]/@content')

        # title
        title_meta = '//meta[contains(@property, "og:title")]/@content'
        title_tag = '//title/text()'
        title_h1 = '//h1/descendant-or-self::text()'
        if response.xpath(title_meta) is not None:
            il.add_xpath('title', title_meta)
        elif response.xpath(title_tag) is not None:
            il.add_xpath('title', title_tag)
        elif response.xpath(title_h1) is not None:
            il.add_xpath('title', title_h1)

        # publish date
        publish_time_meta = '//meta[contains(@property, "aritcle:published_time")]/@content'
        publish_time_class = '//h1/descendant-or-self::text()'
        if response.xpath(publish_time_meta) is not None:
            il.add_xpath('publish_date', publish_time_meta)
        else:
            il.add_xpath('publish_date', publish_time_class)

        # content
        content_class = '//*[contains(@class, "content") or ' \
                        'contains(@id, "content")]' \
                        '/p/descendant-or-self::text()'
        content_p = '//p/descendant-or-self::text()'
        if response.xpath(content_class):
            il.add_xpath('content', content_class)
        else:
            il.add_xpath('content', content_p)

        yield il.load_item()
        # item['title'] = response.css('h1::text').extract()
        # content = response.xpath('//*[contains(@class, "content") or contains(@id, "content")]/p/descendant-or-self::text()').extract()
        # item['content'] = content if len(content) > 0 else response.xpath('//p/descendant-or-self::text()').extract()
        # item['image_url'] = response.css('meta[property="og:image"]::attr(content)').extract()

        # yield item
