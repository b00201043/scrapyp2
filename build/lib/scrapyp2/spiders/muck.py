import scrapy
import pandas as pd
import pymysql
import json
from datetime import datetime
import re


FB_TOKEN = 'EAAJ1Fc6ZAOQ8BAI6EvaeFWflWgtn28EGstc95uev1dCJDlg6WyuB8AdhbAoFFBHUYo2XPUAK8iWP61DOpTr2vt3TIRKORuZBuSK0KXHFlm0DBWoVy2ijxevRMC18Ssv4jaWL1W6f3MFVPVk8BSK2m0Hf5OMHxMKpnW9VVYEQZDZD'
FB_URL = 'https://graph.facebook.com/v2.2/?id=%s&access_token=%s'


class MuckItem(scrapy.Item):
    name=scrapy.Field()
    muck_url=scrapy.Field()
    location=scrapy.Field()
    domain=scrapy.Field()
    title=scrapy.Field()
    beats=scrapy.Field()
    img_url=scrapy.Field()
    social_link=scrapy.Field()


class ArticleItem(scrapy.Item):
    url=scrapy.Field()
    name=scrapy.Field()
    fb=scrapy.Field()
    text=scrapy.Field()
    date=scrapy.Field()
    image_url=scrapy.Field()
    title=scrapy.Field()


def query(sql):
    # decode oarams first
    db = pymysql.connect(host='spark.cippsw3zz0rz.ap-northeast-1.rds.amazonaws.com', port=3306, user='duke', db='amplify', passwd='spark2266', charset='utf8')
    cursor = db.cursor()
    cursor.execute(sql)
    res = cursor.fetchall()
    db.commit()
    cursor.close()
    db.close()
    return res

class MuckSpider(scrapy.Spider):
    custom_settings = {
        "COOKIES_ENABLED": 0,
        "DOWNLOAD_DELAY": 1,
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    }
    name='muck'
    def start_requests(self):
        #sql='select distinct author_name,domain from spark.author limit 70000,10000'
        #rows=query(sql)
        #print('=============%s sql rows=========================' %len(rows))
        #df=pd.read_excel('https://s3-ap-northeast-1.amazonaws.com/scrapinghub-public-files/Product+Research_ConserWater.xlsx',sheetname=1)
        df=pd.read_csv('https://s3-ap-northeast-1.amazonaws.com/scrapinghub-public-files/websummit_name.csv')

        rows=[[df.iloc[i]['name'],df.iloc[i]['domain']]   for i in range(len(df)) ]
        for row in rows:

            item=MuckItem()

            item['name']=row[0].strip('/n /t')
            print(item['name'])
            item['domain']=row[1]

            print('domain %s====='%item['domain'])
            url="https://www.google.com.tw/search?q=%s+site:muckrack.com&aqs=chrome..69i57.12162j0j7&sourceid=chrome&ie=UTF-8"%item['name']
            r=scrapy.Request(url=url,callback=self.google_muck)
            r.meta['item']=item
            yield r


    def google_muck(self,response):
        item = response.meta['item']
        muck_url=response.css('div .rc .r a::attr(href)').extract_first()
        print(muck_url)
        try:
            if len(muck_url)>0:
                if muck_url[0]=='/':
                    item['muck_url']=muck_url[muck_url.find('h'):muck_url.find('&')]
                else:
                    item['muck_url']=muck_url
                r=scrapy.Request(url=item['muck_url'],callback=self.profile)
                r.meta['item']=item
                yield r

            else :
                yield None
        except:
            print('error----%s'%item['name'])



    def profile(self,response):
        item = response.meta['item']
        name=response.css('.profile-name::text').extract_first()

        try:
            if item['name'].lower() in name.lower():
                location=response.css('.person-details-item.person-details-location::text').extract()[1].strip() if response.css('.person-details-item.person-details-location::text').extract() else ''
                title=response.css('.person-details-item.person-details-title::text').extract()[1].strip() if response.css('.person-details-item.person-details-title::text') else ''
                beats=response.css('.person-details-item.person-details-beats a::text').extract()
                img_url=response.css('img.img-circle::attr(src)').extract()
                social_link=response.css('.profile-social-link::attr(href)').extract()

                item['location']=location
                item['title']=title
                item['beats']=beats
                item['img_url']='' if img_url[0].endswith('s=256') else img_url[0]
                item['social_link']=social_link
                print('social link')
                print(social_link)
                #social_link=df.loc[i]['social_link'].split(',') if type(df.loc[i]['social_link'])!=float else ''

                fb=[i for i in social_link if 'facebook.com' in i]
                fb=fb[0] if len(fb)>0 else ''

                twitter=[i for i in social_link if 'twitter.com' in i]
                twitter=twitter[0] if len(twitter)>0 else ''

                linkedin=[i for i in social_link if 'linkedin.com' in i]
                linkedin=linkedin[0] if len(linkedin)>0 else ''

                instagram=[i for i in social_link if 'instagram.com' in i]
                instagram=instagram[0] if len(instagram)>0 else ''

                youtube=[i for i in social_link if 'youtube.com' in i]
                youtube=youtube[0] if len(youtube)>0 else ''

                sql="insert into amplify.profile (media_name, domain, img_url, tags, location, fb, twitter, linkedin , ig, youtube) values (\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\");"
                print(sql%(item['name'], item['domain'], item['img_url'], beats, location, fb, twitter, linkedin, instagram, youtube))
                url = 'https://www.google.com/search?q=' +item["name"]+ ' site:%s 2016.. &tbm=nws&num=100&safe=off&espv=2&biw=1279&bih=571&ei=RnwHV8TwMcG_0AT985ToDw&start=%s&sa=N&filter=0'% item['domain']
                #url = 'https://www.google.com/search?q=\"' +item["name"]+ '\"2016.. &tbm=nws&num=100&safe=off&espv=2&biw=1279&bih=571&ei=RnwHV8TwMcG_0AT985ToDw&start=0&sa=N&filter=0'

                #change date

                try:
                    item_2=ArticleItem()
                    item_2["name"]=item['name']
                    r1=scrapy.Request(url=url%0, callback=self.search_result, dont_filter=True)
                    r2=scrapy.Request(url=url%100, callback=self.search_result, dont_filter=True)

                    r1.meta['item'] = item_2
                    r2.meta['item'] = item_2
                    yield r1
                    yield r2
                    yield item
                except:
                    print('error')
                query(sql%(item['name'], item['domain'], item['img_url'], beats, location, fb, twitter, linkedin, instagram, youtube))
            else:
                url = 'https://www.google.com/search?q=' +item["name"]+ ' site:%s 2016.. &tbm=nws&num=100&safe=off&espv=2&biw=1279&bih=571&ei=RnwHV8TwMcG_0AT985ToDw&start=0&sa=N&filter=0'%item['domain']

                item_2=ArticleItem()
                item_2["name"]=item['name']
                r=scrapy.Request(url=url, callback=self.search_result, dont_filter=True)
                r.meta['item'] = item_2
                yield r
                yield item



        except :
            url = 'https://www.google.com/search?q=' +item["name"]+ ' site:%s 2016.. &tbm=nws&num=100&safe=off&espv=2&biw=1279&bih=571&ei=RnwHV8TwMcG_0AT985ToDw&start=0&sa=N&filter=0'%item['domain']

            item_2=ArticleItem()
            item_2["name"]=item['name']
            r=scrapy.Request(url=url, callback=self.search_result, dont_filter=True)
            r.meta['item'] = item_2
            yield r
            yield item

            yield None
            print(item['name']+'no data')




    def search_result(self,response):


        item = response.meta['item']

        urls =response.css("._hJs a::attr(href)").extract()
        print(urls)
        dates=response.css('div span.f.nsa._QHs::text').extract()
        print('date')
        print(dates)
        for item_d in zip(dates, urls):


            item['url'] = item_d[1]

            try:

                date=datetime.strptime('%s'%item_d[0], '%b %d, %Y').strftime('%Y-%m-%d')
            except:

                try:
                    date=datetime.strptime('%s'%item_d[0], '%d.%m.%Y').strftime('%Y-%m-%d')
                except:
                    date='0000-00-00'

            item['date'] =date
            url=item['url']
            r=scrapy.Request(url=url, callback=self.article, dont_filter=True)
            r.meta['item'] = item

            yield r



    def article(self,response):
        if response.css('[lang|="en"]').extract():
            item = response.meta['item']
            print('article')

            image_metas = response.css('meta[property="og:image"]::attr(content)').extract()
            article_img = image_metas[0] if len(image_metas) > 0 else None
            item['image_url']=article_img

            title=response.css('h1::text').extract()
            title = ' '.join([ele if ele else '' for ele in title])
            item['title'] = title.replace('\"','').replace('\n','').encode('utf-8').decode('utf-8')

            eles = response.xpath(u'//p[normalize-space()]/text()').extract()
            article = (' '.join([ele if ele else '' for ele in eles])).lower()

            item['text'] = article.replace('\"','').replace('\n','').encode('utf-8').decode('utf-8')





            #r = scrapy.Request(url=FB_URL % (item['url'], FB_TOKEN), callback=self.parse_fb_share, dont_filter=True)
            #r.meta['item'] = item
            #yield r
            sql="""insert into amplify.articles (url, name, image_url , date, text, title)
                    values (\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"); """

            item['text'] = re.sub(r"(?:&nbsp;)+", '', item['text'])

            yield item
            print(sql %(item['url'], item['name'], item['image_url'], item['date'], item['text'],item['title']))
            query(sql %(item['url'], item['name'], item['image_url'], item['date'], item['text'],item['title']))

        else:
            yield None


    def parse_fb_share(self, response):
        item = response.meta['item']
        print('fb')
        res_json = json.loads(response.body)
        fb = res_json['share']['share_count'] if 'share' in res_json else None
        item['fb']=fb
        sql="""insert into amplify.articles (url, name, image_url, fb , date, text, title)
                values (\"%s\",\"%s\",\"%s\",%s,\"%s\",\"%s\",\"%s\"); """

        yield item
        item['text'] = re.sub(r"(?:&nbsp;)+", '', item['text'])
        print(item['text'])
        print(sql %(item['url'], item['name'], item['image_url'], item['fb'], item['date'], item['text'],item['title']))
        query(sql %(item['url'], item['name'], item['image_url'], item['fb'], item['date'], item['text'],item['title']))
        print('sql finish')





    #df2=pd.read_csv('/Users/duke/SA/scrapyp2/TechCrunch Disrupt SF 2016 - Confirmed Media_Duke.csv')



        #domain=df2.loc[(df2['FIRST NAME']+' '+df2['LAST NAME'])==name]
        #domain=domain['OUTLET'].values[0] if domain['OUTLET'].values[0] else ''
