"""
    2017-09-21 @ chi
    A spider to parse the google search result bootstraped from given queries.
"""
from w3lib.html import remove_tags, replace_entities, replace_escape_chars
import re
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from urllib.parse import urljoin, urlparse, parse_qsl
from datetime import datetime
from scrapy.spiders import Spider
from scrapy.http import Request
from scrapy.utils.response import get_base_url
from scrapy.utils.misc import arg_to_iter
from scrapy.item import Item, Field
import re
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from w3lib.html import remove_tags, replace_entities, replace_escape_chars
import json
import scrapy
from difflib import SequenceMatcher

def _parse_url(href):
    """
    parse the website from anchor href.

    for example:

    >>> _parse_url(u'/url?q=http://www.getmore.com.hk/page18.php&sa=U&ei=Xmd_UdqBEtGy4AO254GIDg&ved=0CDQQFjAGODw&usg=AFQjCNH08dgfL10dJVCyQjfu_1DEyhiMHQ')
    u'http://www.getmore.com.hk/page18.php'
    """
    queries = dict(parse_qsl(urlparse(href).query))
    return queries.get('q', '')

def clean_ascii(value):
    clean_ascii = r'[^\x00-\x7f]+'
    rtn_string = re.sub(clean_ascii, r'', value)
    return rtn_string


def normal_content(value):
    regex_sentence = r'([A-Z][^\.!?]*[\.!?])'
    rtn_string = re.findall(regex_sentence, value)
    return rtn_string


class EasyArticleItem(Item):
    image_url = Field(output_processor=TakeFirst())
    url=Field()
    title = Field(
        input_processor=MapCompose(remove_tags, replace_entities, replace_escape_chars,
                                   clean_ascii),
        output_processor=Join()
    )
    publish_date = Field()

    domain=Field()
    content = Field(
        input_processor=MapCompose(remove_tags, replace_entities,
                                   replace_escape_chars,
                                   clean_ascii, normal_content
                                   ),
        output_processor=Join()
    )
    fb=Field()
    google=Field()
    twitter=Field()
    linkedin=Field()
    author_source=Field(output_processor=TakeFirst())
    author_source_link=Field(output_processor=TakeFirst())
    links=Field()
    route=Field()


class GoogleSearchItem(Item):
    name = Field()
    url = Field()
    html = Field()
    query = Field()
    crawled = Field()

class MuckItem(Item):
    name = Field()
    muck_url = Field()
    location = Field()
    domain = Field()
    title = Field()
    beats = Field()
    img_url = Field()
    social_link = Field()



class requestItem(Item):
    url = Field()

class GoogleSearchSpider(Spider):
    name = 'googleSearch'
    queries = ('machine learning')
    #site = 'itpison.com'
    site=''
    tbm = 'nws'
    hl = 'en'
    lr = 'lang_en'
    num = 100
    safe = 'off'
    as_qdr = ''
    base_url_fmt = 'http://www.google.com/search?' \
                   'q={query}&as_sitesearch={site}&tbm={tbm}' \
                   '&hl={hl}&lr={lr}' \
                   '&num={num}&safe={safe}' \
                   '&as_qdr={as_qdr}'
                   #'&tbs=cdr:1,cd_min:10/1/2016,cd_max:1/1/2017'
    download_html = False



#========1-1 part==============================

    def start_requests(self):
        print(self.queries)
        for query in arg_to_iter(self.queries):
            query = '+'.join(query.split()).strip('+')
            url = self.base_url_fmt.format(query=query,
                                           site=self.site,
                                           tbm=self.tbm,
                                           hl=self.hl,
                                           lr=self.lr,
                                           num=self.num,
                                           safe=self.safe,
                                           as_qdr=self.as_qdr)
            yield Request(url=url, meta={'query': query},callback=self.parse)
        pass
#=============1-2=====2-3==============================
    def parse(self, response):
        print("=====parse===========================")
        try:

            item = response.meta['item']

        except:
            item=EasyArticleItem()
            item['route']=1

        dates=response.css('div span.f.nsa._QHs::text').extract()
        #print(dates)
        print(response.xpath('//div[@id="ires"]').extract()[0])
        for i,sel in enumerate(response.xpath('//div[@id="ires"]')):
            dates=sel.css('div span.f.nsa._QHs::text').extract()
            name = u''.join(sel.xpath(".//text()").extract())
            url = _parse_url(sel.xpath('.//a/@href').extract()[0])
            
            if len(url):
                r=Request(url=url,callback=self.article_parse)
                item['url']=url

                item['publish_date']=dates
                try:
                    if 'hours' or 'minutes' in  item['publish_date']:
                        item['publish_date']=datetime.now().strftime('%Y-%m-%d')
                    else:
                        item['publish_date']=datetime.strptime('%s'%item['publish_date'], '%b %d, %Y').strftime('%Y-%m-%d')
                except:

                    try:
                        item['publish_date']=datetime.strptime('%s'%item['publish_date'], '%d.%m.%Y').strftime('%Y-%m-%d')
                    except:
                        item['publish_date']='0000-00-00'
                r.meta['item'] = item
                yield r

        next_page = response.xpath('//table[@id="nav"]//td[contains(@class, "b") and position() = last()]/a')


        if next_page:
            url = self._build_absolute_url(response, next_page.xpath('.//@href').extract()[0])
            yield Request(url=url, callback=self.parse)

    def parse_item(self, response):

        name = response.meta['name']
        query = response.meta['query']
        url = response.url
        html = response.body[:1024 * 256]
        timestamp = datetime.datetime.utcnow().isoformat()

        #yield requestItem({'url': url})
        yield GoogleSearchItem({'name': name,
                                'url': url,
                                'html': html,
                                'query': query,
                                'crawled': timestamp})


    def _build_absolute_url(self, response, url):
        return urljoin(get_base_url(response), url)

#===========1-3===============================
    def article_parse(self, response):
        print("======article part ======================")

        item=response.meta['item']
        il = ItemLoader(item=item, response=response)


        # image url
        il.add_xpath('image_url', '//meta[contains(@property, "og:image")]/@content')

        # title
        title_meta = '//meta[contains(@property, "og:title")]/@content'
        title_tag = '//title/text()'
        title_h1 = '//h1/descendant-or-self::text()'
        if response.xpath(title_meta) is not None:
            il.add_xpath('title', title_meta)
        elif response.xpath(title_tag) is not None:
            il.add_xpath('title', title_tag)
        elif response.xpath(title_h1) is not None:
            il.add_xpath('title', title_h1)

        # publish date
        publish_time_meta = '//meta[contains(@property, "aritcle:published_time")]/@content'
        publish_time_class = '//h1/descendant-or-self::text()'
        if response.xpath(publish_time_meta) is not None:
            il.add_xpath('publish_date', publish_time_meta)

        else:
            il.add_xpath('publish_date', publish_time_class)

        # content
        content_class = '//*[contains(@class, "content") or ' \
                        'contains(@id, "content")]' \
                        '/p/descendant-or-self::text()'
        content_p = '//p/descendant-or-self::text()'
        if response.xpath(content_class):
            il.add_xpath('content', content_class)
        else:
            il.add_xpath('content', content_p)


        #author catchers
        RULES = ['a.author', 'a[rel=author]', 'a[href*=author]',
                 '.post-author .fn', '.byline a', '.author-name',
                 '.c-byline__item a', 'span.author', '.post-author > .fn',
                 'a[itemprop=author]', '.author a']
        # author_schema = './/*[contains(@itemtype, "Person")]/node()'
        author_schema = './/*[contains(@itemprop, "author")]/node()'
        author_meta = '//meta[contains(@property, "aritcle:publisher")]/@content'

        il.add_value('author_source_link','')
        author_class = '//*[contains(@class, "author")]/node()'
        if response.xpath(author_schema):
             il.add_xpath('author_source', '%s/text()'% author_schema)
             il.add_xpath('author_source_link', '%s/@href'% author_schema)




        elif response.xpath(author_meta):
             il.add_xpath('author_source', '%s/text()'% author_meta)
             il.add_xpath('author_source_link', '%s/@href'% author_meta)

        else:
            for name in RULES:

                if response.css('%s::text' %name):
                    il.add_css('author_source','%s::text' %name)
                    il.add_css('author_source_link','%s::attr(href)' %name)

                    break

                elif name=='.author a':
                     if response.xpath(author_class):
                        il.add_xpath('author_source', '%s/text()' % author_class)
                        il.add_xpath('author_source_link', '%s/@href' % author_class)

                        break



        if il.load_item()['route']==2 or 'author_source_link' not in il.load_item():
            print("==============route2  no social link proccess=============")
            api_url='http://free.donreach.com/shares?providers=facebook,google,twitter,linkedin&url='
            headers={
            'Authorization':'26062b538e93459e73b39702128b27c7'

            }
            url=il.load_item()['url']
            r=Request(url=api_url+url,headers=headers,callback=self.social_parse)

            r.meta['item']=il.load_item()
            yield r

        else:
            print("=========route1 to author_profile===================")
            domain=il.load_item()['url'].split('/')[2]
            il.load_item()['domain']=domain

            link=il.load_item()['author_source_link']
            if type(link)==list:
                link=link[0] if '/' in link[0] else link[1]

            link = link if link.startswith('http') else 'http://' + domain + link
            r=Request(url=link,callback=self.parse_author_profile)
            r.meta['item']=il.load_item()
            yield r


#=================1-3-2=====================================
    def parse_author_profile(self, response):
        print("====================parse=============================")
        item = response.meta['item']
        def is_social_link(target):
            SOCIAL_DOMAINS = ['facebook.com', 'twitter.com', 'linkedin.com']
            if not target:
                return False
            return any([target.find(sdomain) != -1 for sdomain in SOCIAL_DOMAINS])


        #===================比對是否為domain   social media
        def is_company(link,domain):
            domain=domain.split('.')[0] if domain.split('.')[0]!='www' else domain.split('.')[1]
            if link.find(domain)!=-1 or not link:
                return False
            return True

        def like_name(link,name):
            if type(link)==str and type(name)==str:
                if max(SequenceMatcher(None, link.split('/')[-1], name).ratio(),SequenceMatcher(None, link.split('/')[-2], name).ratio())>0.8:
                    return True
            return False


        if 'author_source' in item:
            links = set([target for target in response.css('a::attr(href)').extract() if is_social_link(target) and is_company(target,item['domain']) and like_name(target,item['author_source'])])
            links_str = ':::'.join(links)
            item['links']=links_str

        #call social share api
        url=item['url']
        api_url='http://free.donreach.com/shares?providers=facebook,google,twitter,linkedin&url='
        headers={
        'Authorization':'26062b538e93459e73b39702128b27c7'

        }
        r=Request(url=api_url+url,headers=headers,callback=self.social_parse)
        r.meta['item']=item
        yield r

#================1-4==============================
    def social_parse(self,response):
        print("=======social_parse part==============")

        item=response.meta['item']
        #print(response.body)
        sshare=json.loads(response.body)

        item['fb']=sshare['shares']['facebook']
        item['google']=sshare['shares']['google']
        item['twitter']=sshare['shares']['twitter']
        item['linkedin']=sshare['shares']['linkedin']


        item_2=MuckItem()

        try:
            item_2['name']=item['author_source']
            item_2['domain']=item['domain']
            item_2['social_link']=item['links']
            url="https://www.google.com.tw/search?q=%s+site:muckrack.com&aqs=chrome..69i57.12162j0j7&sourceid=chrome&ie=UTF-8" % item_2['name']
            r=scrapy.Request(url=url,callback=self.google_muck)
            r.meta['item']=item_2
            if item['route']==1:
                yield r
            else:
                yield item
        except:
            yield None

        yield item


#====================2-1===================================
    def google_muck(self,response):
        print('===========start muck rack===================')
        item = response.meta['item']
        muck_url=response.css('div .rc .r a::attr(href)').extract_first()

        try:
            if len(muck_url)>0:
                if muck_url[0]=='/':
                    item['muck_url']=muck_url[muck_url.find('h'):muck_url.find('&')]
                else:
                    item['muck_url']=muck_url
                r=Request(url=item['muck_url'],callback=self.profile)
                r.meta['item']=item
                yield r

            else:
                yield None
        except:
            print('error----%s'%item['name'])
#===============2-2================================
    def profile(self,response):
        item = response.meta['item']
        name=response.css('.profile-name::text').extract_first()

        try:
            if item['name'].lower() in name.lower():
                location=response.css('.person-details-item.person-details-location::text').extract()[1].strip() if response.css('.person-details-item.person-details-location::text').extract() else ''
                title=response.css('.person-details-item.person-details-title::text').extract()[1].strip() if response.css('.person-details-item.person-details-title::text') else ''
                beats=response.css('.person-details-item.person-details-beats a::text').extract()
                img_url=response.css('img.img-circle::attr(src)').extract()
                social_link=response.css('.profile-social-link::attr(href)').extract()

                item['location']=location
                item['title']=title
                item['beats']=beats
                item['img_url']='' if img_url[0].endswith('s=256') else img_url[0]
                item['social_link']=social_link
                print('social link')
                print(social_link)
                #social_link=df.loc[i]['social_link'].split(',') if type(df.loc[i]['social_link'])!=float else ''

                fb=[i for i in social_link if 'facebook.com' in i]
                fb=fb[0] if len(fb)>0 else ''

                twitter=[i for i in social_link if 'twitter.com' in i]
                twitter=twitter[0] if len(twitter)>0 else ''

                linkedin=[i for i in social_link if 'linkedin.com' in i]
                linkedin=linkedin[0] if len(linkedin)>0 else ''

                instagram=[i for i in social_link if 'instagram.com' in i]
                instagram=instagram[0] if len(instagram)>0 else ''

                youtube=[i for i in social_link if 'youtube.com' in i]
                youtube=youtube[0] if len(youtube)>0 else ''

                url = 'https://www.google.com/search?q=' +item["name"]+ ' site:%s 2016.. &tbm=nws&num=100&safe=off&espv=2&biw=1279&bih=571&ei=RnwHV8TwMcG_0AT985ToDw&start=0&sa=N&filter=0'% item['domain']
                #url = 'https://www.google.com/search?q=\"' +item["name"]+ '\"2016.. &tbm=nws&num=100&safe=off&espv=2&biw=1279&bih=571&ei=RnwHV8TwMcG_0AT985ToDw&start=0&sa=N&filter=0'



                item_2=EasyArticleItem()
                item_2['author_source']=item['name']
                item_2['domain']=item['domain']
                item_2['route']=2
                r1=scrapy.Request(url=url, callback=self.parse, dont_filter=True)
                r1.meta['item'] = item_2
                yield r1
                yield item
        except:
            try:
                twitter_link=[link for link in item['social_link'].split(':::') if 'twitter' in link][0]
                r=Request(url=twitter_link,callback=self.twitter_parse)
                r.meta['item']=item
                yield r
            except:

                print('no %s '%item['name'])
#=========2-3=====================================

    def twitter_parse(self,response):
        item=response.meta['item']
        item['img_url']=response.css('.ProfileAvatar-image::attr(src)').extract()
        item['location']=response.css('div .ProfileHeaderCard-location a::text').extract()

        yield item
