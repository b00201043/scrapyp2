"""Utility to interact to DB"""

import pymysql


def query(sql, params=()):
    # decode oarams first
    db = pymysql.connect(host='spark.cippsw3zz0rz.ap-northeast-1.rds.amazonaws.com', port=3306, user='duke', db='amplify', passwd='spark2266', charset='utf8')
    cursor = db.cursor()
    cursor.execute(sql, params)
    db.commit()
    row = cursor.fetchone()
    while row is not None:
        yield row
        row = cursor.fetchone()
    cursor.close()
    db.close()
    yield None
