import scrapy

from .. import db

class imgItem(scrapy.Item):
    name=scrapy.Field()
    media_id=scrapy.Field()
    img_url=scrapy.Field()


class TrySpider(scrapy.Spider):
    name='try'
    custom_settings = {
        "COOKIES_ENABLED": 0,
        "DOWNLOAD_DELAY": 0.75,
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    }
    def start_requests(self):
        rows=db.query("select media_name,twitter,media_id from amplify.profile where twitter !='' ;")

        for r in rows:
            item=imgItem()
            item['name']=r[0]
            item['media_id']=r[2]

            r=scrapy.Request(url=r[1],callback=self.parse)
            r.meta['item']=item
            yield r


    def parse(self,response):
        item=response.meta['item']
        item['img_url']=response.css('.ProfileAvatar-image::attr(src)').extract()
        print(item)
        print("======insert %s "%item['name'])

        #db.query("update amplify.profile set img_url=\'%s\' where media_id=%s" %(item['img_url'],item['media_id']))

        yield item
